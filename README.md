# Devocional

¹⁰ O que faz coisas grandes e inescrutáveis; e maravilhas sem número. 

Jó 9:10
---

³ Bendito o Deus e Pai de nosso Senhor Jesus Cristo, o qual nos abençoou com todas as bênçãos espirituais nos lugares celestiais em Cristo; 

Efésios 1:3
---

⁸ Abre a tua boca a favor do mudo, pela causa de todos que são designados à destruição. 

Provérbios 31:8
---

³ Lembrai-vos dos presos, como se estivésseis presos com eles, e dos maltratados, como sendo-o vós mesmos também no corpo. 

Hebreus 13:3